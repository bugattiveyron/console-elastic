﻿using SwElasticSearch.Facade;
using SwElasticSearch.Model;

namespace SwElasticSearch.Interface
{
    public class IIndexFacade : IndexFacade
    {
        void Indexar(AccountLogged message);
    }
}

﻿using SwElasticSearch.Interface;
using SwElasticSearch.Model;

namespace SwElasticSearch.Facade
{
    public class IndexFacade 
    {
        //private IElasticClient _client;

        //public IndexFacade(IElasticClient client)
        //{
        //    _client = client;
        //}
        public IndexFacade()
        {
            
        }

        public void Indexar(AccountLogged message)
        {
            var service = ElasticServiceConfiguration.GetInstance();
            service.Index(message, idx => idx.Index(ElasticServiceConfiguration.GetIndexName()));
        }
    }
}

﻿using SwElasticSearch.Facade;
using SwElasticSearch.Interface;
using System;

namespace SwElasticSearch
{
    public class Program
    {
        private static IIndexFacade _indexFacade;

        public static void Main(string[] args)
        {
            var message = new Model.AccountLogged()
            {
                Id = 4,
                Name = "Monica",
                PostDate = new DateTime(2016, 007, 13),
                Message = "I am tring"
            };
            
            _indexFacade.Indexar(message);
        }

        private static void PrintMessage(string indice, string id)
        {
            Console.WriteLine("-----------------------------------------------------");
            Console.WriteLine("Indexado: " + id);
            Console.WriteLine("Indice: " + indice);
            Console.WriteLine("-----------------------------------------------------");
        }
    }
}

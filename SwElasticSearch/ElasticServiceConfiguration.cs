﻿using Nest;
using System;
using System.Configuration;

namespace SwElasticSearch
{
    public static class ElasticServiceConfiguration
    {
        private static Uri node = new Uri(GetIndexService());
        private static ConnectionSettings settings = new ConnectionSettings(node);
        private static IElasticClient _client;
        
        /// <summary>
        /// Instância SingleTom
        /// </summary>
        /// <returns></returns>
        public static IElasticClient GetInstance()
        {
            if (_client == null)
            {
                _client = new ElasticClient(settings);
                return _client;
            }

            return _client;
        }

        /// <summary>
        /// Name Index
        /// </summary>
        /// <returns></returns>
        public static string GetIndexName()
        {
            return ConfigurationSettings.AppSettings["INDEX-NAME"];
        }

        /// <summary>
        /// Name Server Elastic
        /// </summary>
        /// <returns></returns>
        public static string GetIndexService()
        {
            return ConfigurationSettings.AppSettings["INDEX-SERVER"];
        }

    }
}

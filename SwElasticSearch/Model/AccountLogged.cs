﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SwElasticSearch.Model
{
    public class AccountLogged
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime PostDate { get; set; }

        public string Message { get; set; }
    }
}
